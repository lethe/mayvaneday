# A Nomadic Manifesto
## or, Survival on the Modern Internet

1. Modern corporations in the technology sector exist mainly due to being able to monetize their users' data.

2. Staying in one place on the internet for too long encourages cruft to build up and a data profile to be more easily made.

3. For those who do not consent to the most intimate details of their life being commodized, there is no permanent safe haven for us in this world. We are condemned to wandering forever.

Do not be afraid of death, for rebirth follows after.

[original inspiration](http://web.archive.org/web/20200224225900/https://gopher.tildeverse.org/grex.org:70/0/~papa/pgphlog/2018/alm-Antisocial_Media_Manifesto)
