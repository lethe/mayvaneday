/*
Script to block known unauthorized mirrors
written by Vane Vander <https://mayvaneday.org>
released under MIT License
*/

let text = window.location.href;
let nun = text.includes("nun7gbqj6tro");
if ((nun === true)) {
    window.location.replace("https://theannoyingsite.com");
}
