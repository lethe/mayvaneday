# The big list of women who did things

<!-- for future reference: we are alphabetizing by LAST name. Only first if no surname -->

Unfinished, obviously.

Last updated: 2025-02-15

## Art
- [Aphra Behn](https://en.wikipedia.org/wiki/Aphra_Behn): one of the first English women to earn her living by her writing
- [Tina Bell](https://en.wikipedia.org/wiki/Tina_Bell): pioneer of the "grunge" music genre, before Nirvana
- [Lisa Ben](https://en.wikipedia.org/wiki/Lisa_Ben): created *Vice Versa*, the first known lesbian publication in North America
- [Anne Bradstreet](https://en.wikipedia.org/wiki/Anne_Bradstreet): first writer to be published in England's North American colonies
- [Diemoth](https://en.wikipedia.org/wiki/Diemoth): 12th-century recluse who transcribed at least forty-five manuscripts; notable for her "beautiful handwriting"
- [Emilia Lanier](https://en.wikipedia.org/wiki/Emilia_Lanier): first known woman in England to declare herself a poet
- [Enheduanna](https://en.wikipedia.org/wiki/Enheduanna): first known poet
- [Herrad of Landsberg](https://en.wikipedia.org/wiki/Herrad_of_Landsberg): 12th-century nun who wrote an early pictoral encyclopedia, *Hortus deliciarum*, to teach the women in her convent about the sciences of the time
- [Hrotsvitha](https://en.wikipedia.org/wiki/Hrotsvitha): first female writer (from German-speaking lands) and first female historian
- [Baya Mahieddine](https://en.wikipedia.org/wiki/Baya_(artist)): surrealist artist whose work was imitated by Picasso
- [Marie de France](https://archive.md/https://www.britannica.com/biography/Marie-de-France): earliest known French woman poet
- [Edna St. Vincent Millay](https://wikipedia.org/wiki/Edna_St._Vincent_Millay?lang=en): first woman (and second person!) to win the Pulitzer Prize for Poetry
- [Julian of Norwich](https://en.wikipedia.org/wiki/Julian_of_Norwich): author of *Revelations of Divine Love*, the earliest surviving English-language works attributed to a woman
- [Lucy Terry Prince](https://en.wikipedia.org/wiki/Lucy_Terry): author of *Bars Fight*, the oldest known work of literature by an African American
- [Lotte Reiniger](https://en.m.wikipedia.org/wiki/Lotte_Reiniger): director/writer of the oldest surviving animated feature film, [_The Adventures of Prince Achmed_](https://en.m.wikipedia.org/wiki/The_Adventures_of_Prince_Achmed)
- [Jane Johnston Schoolcraft](https://en.wikipedia.org/wiki/Jane_Johnston_Schoolcraft): one of the earliet known Native American literary writers
- [Mary Shelley](https://en.wikipedia.org/wiki/Mary_Shelley): novelist and pioneer of the science fiction genre of books
- [Murasaki Shikibu](https://en.wikipedia.org/wiki/Murasaki_Shikibu): wrote the first novel, *The Tale of Genji*
- [Pamela Colman Smith](https://en.wikipedia.org/wiki/Pamela_Colman_Smith): illustrator of the iconic Rider-Waite tarot deck
- [Valerie Thomas](https://en.wikipedia.org/wiki/Valerie_Thomas): invented the illusion transmitter, critical for the invention of 3D movies
- [Maud Wagner](https://wikipedia.org/wiki/Maud_Wagner): first female tattoo artist in the USA

[More information](https://www.girlmuseum.org/project/girl-authors/)

## Computers
- [Ada Lovelace](https://wikipedia.org/wiki/Ada_Lovelace): the first computer programmer
- Kathleen Booth: inventor of assembly language
- [Evelyn Berezin](https://en.m.wikipedia.org/wiki/Evelyn_Berezin): designed the first computer word proecssor
- [Edith Clarke](https://wikipedia.org/wiki/Edith_Clarke?lang=en): first woman to earn a degree in electrical engineering
- [Marian Croak](https://wikipedia.org/wiki/Marian_Croak?lang=en): invented VoIP (Voice over Internet Protocol)
- [Judith Estrin](https://en.wikipedia.org/wiki/Judith_Estrin): instrumental in the invention of TCP
- [Margaret Hamilton](https://en.wikipedia.org/wiki/Margaret_Hamilton_(software_engineer)): lead programmer on the Apollo project
- [Grace Hopper](https://en.wikipedia.org/wiki/Grace_Hopper): created the first compiler for a programming language
- [Susan Kare](https://en.wikipedia.org/wiki/Susan_Kare): "pioneer of pixel art"; designed many of the icons, fonts, and images for Apple, NeXT, and IBM in the 1980s
- [Hedy Lamarr](https://en.wikipedia.org/wiki/Hedy_Lamarr#Inventor): invented Wi-Fi
- [Ruth Teitelbaum](https://web.archive.org/web/20221129021942/https://spectrum.ieee.org/the-women-behind-eniac): one of the six women who programmed ENIAC
- [Gladys West](https://en.wikipedia.org/wiki/Gladys_West): mathematician who developed the satellite geodesy models eventually developed into the Global Positioning System (GPS)

[More information](http://ybgg2evrcdz37y2qes23ff3wjqjdn33tthgoagi76vhxytu4mpxiz5qd.onion/wiki/Women_in_computing?lang=en)

## Industry
- [Tabitha Babbitt](https://en.wikipedia.org/wiki/Tabitha_Babbitt): invented the circular saw
- [Katharine Burr Blodgett](https://en.wikipedia.org/wiki/Katharine_Burr_Blodgett): invented non-reflective (invisible) glass
- [Sarah Boone](https://wikipedia.org/wiki/Sarah_Boone?lang=en): invented the modern ironing board
- [Josephine Cochrane](https://en.wikipedia.org/wiki/Josephine_Cochrane): invented first commercially successful dishwasher
- [Bette Nesmith Graham](https://en.wikipedia.org/wiki/Bette_Nesmith_Graham): invented Liquid Paper
- [Margaret Knight](https://wikipedia.org/wiki/Margaret_E._Knight): invented the paper bag machine
- [Stephanie Kwolek](https://en.wikipedia.org/wiki/Stephanie_Kwolek): invented Kevlar
- [Alice H. Parker](https://wikipedia.org/wiki/Alice_H._Parker?lang=en): invented central heating

## Medicine
- [June Almeida](https://wikipedia.org/wiki/June_Almeida): discovered the coronavirus group of viruses
- [Leila Denmark](https://wikipedia.org/wiki/Leila_Denmark): synthesised the first vaccine for pertussis (whooping cough)
- [Gertrude Elion](https://wikipedia.org/wiki/Gertrude_B._Elion?lang=en): biochemist and Nobel Prize winner instrumental in the creation of the first antiviral drug widely used to fight AIDS
- [Rosalind Franklin](https://wikipedia.org/wiki/Rosalind_Franklin): discovered the double-helix formation of DNA
- [Mary-Claire King](https://en.wikipedia.org/wiki/Mary-Claire_King): discovered the BRCA1 gene and its role in causing breast cancer
- [Barbara McClintock](https://en.wikipedia.org/wiki/Barbara_McClintock): discovered that genes can move between chromosomes
- [Rita Levi-Montalcini](https://en.wikipedia.org/wiki/Rita_Levi-Montalcini): discovered nerve growth factor
- [Andromachi Papanikolaou](https://en.m.wikipedia.org/wiki/Andromachi_Papanikolaou): key factor in the development of the pap smear test
- [Candace Pert](https://wikipedia.org/wiki/Candace_Pert): discovered the opiate receptor in the brain
- [Susan La Flesche Picotte](https://wikipedia.org/wiki/Susan_La_Flesche_Picotte?lang=en): the first Indigenous woman to earn a medical degree 
- [Mildred Catherine Rebstock](https://en.wikipedia.org/wiki/Mildred_Rebstock): first person to synthesise chloromycetin (an antibiotic)
- [Trota of Salerno](https://wikipedia.org/wiki/Trota_of_Salerno): first gynecologist
- [Nettie Stevens](https://en.wikipedia.org/wiki/Nettie_Stevens): discovered that chromosomes determine sex
- [Flossie Wong-Staal](https://wikipedia.org/wiki/Flossie_Wong-Staal): proved that HIV causes AIDS

## Science
- [Patricia Bath](https://en.wikipedia.org/wiki/Patricia_Bath): inventor of the Laserphaco Probe
- [Ethel Bauer](https://web.archive.org/web/20220716220051/https://www.nasa.gov/centers/marshall/history/ethel-heinecke-bauer.html): planned lunar trajectories for the Apollo program, critical to 13's safe return
- [Katie Bouman](https://wikipedia.org/wiki/Katie_Bouman?lang=en): led the development of the algorithm that took the first image of a black hole
- [Jocelyn Bell Burnell](https://en.wikipedia.org/wiki/Jocelyn_Bell_Burnell): discovered first pulsar
- [Annie Jump Cannon](https://en.wikipedia.org/wiki/Annie_Jump_Cannon): developed the first stellar classification system; classified almost 400,000 stars
- [Eunice Newton Foote](https://en.wikipedia.org/wiki/Eunice_Newton_Foote): discovered the greenhouse effect
- [Cecilia Payne-Gaposchkin](https://wikipedia.org/wiki/Cecilia_Payne-Gaposchkin?lang=en): discovered what stars are made out of
- [Sophie Germain](https://en.wikipedia.org/wiki/Sophie_Germain): French mathematician and pioneer of elasticity theory
- [Fanny Hesse](https://en.wikipedia.org/wiki/Fanny_Hesse): critical in the invention of the Petri dish by suggesting to use agar instead of gelatin
- [Hypatia](https://en.wikipedia.org/wiki/Hypatia): first female mathematician whose life is "reasonably well recorded"
- [Mary Jackson](https://en.wikipedia.org/wiki/Mary_Jackson_(engineer)): NASA's first Black female engineer
- [Mae Jemison](https://en.wikipedia.org/wiki/Mae_Jemison): first Black woman in space
- [Katherine Johnson](https://en.wikipedia.org/wiki/Katherine_Johnson): mathematician crucial for the success of the first USA spaceflights
- [Mary Kenner](https://wikipedia.org/wiki/Mary_Kenner): inventor of the menstrual pad; holder of the record for the most patents awarded to a Black woman in the USA
- [Inge Lehmann](https://en.wikipedia.org/wiki/Inge_Lehmann): discovered the Earth has a solid inner core
- [Rita Levi-Montalcini](https://en.wikipedia.org/wiki/Rita_Levi-Montalcini): discovered nerve growth factor
- [Lise Meitner](https://wikipedia.org/wiki/Lise_Meitner): discovered nuclear fission
- [Maria Sibylla Merian](https://en.wikipedia.org/wiki/Maria_Sibylla_Merian): one of the first naturalists to observe insects directly
- [Mary Sherman Morgan](https://en.wikipedia.org/wiki/Mary_Sherman_Morgan): invented hydyne (a liquid rocket fuel)
- [Emmy Noether](https://en.wikipedia.org/wiki/Emmy_Noether): mathematician; discovered her namesake First and Second Theorems (fundamental in mathematical physics)
- [Cecilia Payne](https://en.wikipedia.org/wiki/Cecilia_Payne-Gaposchkin): discovered what the universe and sun are made of
- [Clarice Phelps](https://en.wikipedia.org/wiki/Clarice_Phelps): part of the team that discovered element 117 (tennessine); first Black woman involved with the discovery of a chemical element
- [Vera Rubin](https://en.wikipedia.org/wiki/Vera_Rubin): discovered dark matter
- [Seondeok of Silla](https://en.wikipedia.org/wiki/Queen_Seondeok_of_Silla): set up first astronomy tower in Asia
- [Donna Strickland](https://en.wikipedia.org/wiki/Donna_Strickland): winner of the 2018 Nobel Prize in Physics for the practical implementation of chirped pulse amplification
- [Judy Sullivan](https://en.wikipedia.org/wiki/Judy_Sullivan): biomedical engineer for the Apollo 11 spaceflight
- [Maria Telkes](https://en.wikipedia.org/wiki/M%C3%A1ria_Telkes): co-built first solar-power-heated heated home with Eleanor Raymond
- [Marie Tharp](https://en.wikipedia.org/wiki/Marie_Tharp): mapped the floor of the Atlantic Ocean; proved the theory of continential drift
- [Chien-Shiung Wu](https://en.wikipedia.org/wiki/Chien-Shiung_Wu): first woman to become president of the American Physical Society; worked on the Manhattan Project and proved that parity is not conserved
- [Maryna Viazovska](https://wikipedia.org/wiki/Maryna_Viazovska?lang=en): solved the sphere packing problem in dimensions 8 and 24
- [...and many more](https://en.m.wikipedia.org/wiki/List_of_female_scientists_before_the_20th_century)

## Social Change
- [Claudette Colvin](https://en.m.wikipedia.org/wiki/Claudette_Colvin): pioneer of the 1950s civil rights movement; refused to give up her bus seat nine months before Rosa Parks did
- [Ruth Ellis](https://en.wikipedia.org/wiki/Ruth_Ellis_(activist)): LGBT rights activist; oldest known surviving open lesbian
- [Barbara Gittings](https://en.wikipedia.org/wiki/Barbara_Gittings): "mother of the gay rights movement"; part of the movement to get the APA to drop homosexuality as a mental illness
- [Temple Grandin](https://en.wikipedia.org/wiki/Temple_Grandin): one of the first public figures to publically come out as Autistic; animal welfare activist that campaigned for humane treatment of cattle in beef processing plants
- [Maura Healey](https://wikipedia.org/wiki/Maura_Healey?lang=en): first open lesbian elected attorney general of a USA state, one of the first LGBT governors of a USA state, and first woman elected governor of the state of Massachusetts
- [Judith Heumann](https://en.wikipedia.org/wiki/Judith_Heumann): held the longest sit-in in a government building for the enactment of Section 504 of the Rehabilitation Act of 1973 (civil rights protection for disabled people)
- [Qiu Jin](https://archive.md/rhArg): Chinese revolutionary, poet, and early feminist
- [Elizabeth Peratrovich](https://wikipedia.org/wiki/Elizabeth_Peratrovich?lang=en): instrumental in the passing of the USA's first anti-discrimination law
- [Hannah Szenes](https://en.wikipedia.org/wiki/Hannah_Szenes): poet and volunteer parachutist who helped evacuate Hungarian Jews during the Holocaust
- [Grunya Sukhareva](https://en.wikipedia.org/wiki/Grunya_Sukhareva): child psychiatrist; first to publish a detailed description of autistic symptoms
- [Mary Edwards Walker](https://en.wikipedia.org/wiki/Mary_Edwards_Walker): only woman to receive the Medal of Honor

## Misc
- [Marie Van Brittan Brown](https://en.wikipedia.org/wiki/Marie_Van_Brittan_Brown): co-invented home security surveillance
- [Bessie Coleman](https://en.wikipedia.org/wiki/Bessie_Coleman): first Black woman to earn an international pilot's license; first known female aviator
- [Martha Gellhorn](https://en.wikipedia.org/wiki/Martha_Gellhorn): journalist and WWII war correspondent; pretended to be a nurse to be the only woman at Normandy on D-Day
- [Helen Lewis](https://en.wikipedia.org/wiki/Helen_Lewis_(journalist)): journalist and namesake of "Lewis' Law": "the comments on any article about feminism justify feminism"
- [Elizabeth Magie](https://wikipedia.org/wiki/Lizzie_Maggie): created the progenitor to the *Monopoly* board game
- [Milunka Savić](https://en.wikipedia.org/wiki/Milunka_Savi%C4%87): most-decorated female combatant in the known history of warfare
- [Rose Valland](https://en.wikipedia.org/wiki/Rose_Valland): captain in the French military during WWII; saved thousands of works of art from being stolen by the Nazis
- [Madam C. J. Walker](https://wikipedia.org/wiki/Madam_C._J._Walker?lang=en): entrepreneur and first female self-made millionaire in the USA
