<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Considering software harmful considered harmful - Archive - MayVaneDay Studios</title>
		<link href="../../../style.css" rel="stylesheet" type="text/css" media="all">
		<meta name="author" content="Vane Vander">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body class="mayvaneday">
		<script src="../../../checktor.js"></script>
		<article>
			<div class="box">
				<p><h1>Considering software harmful considered harmful</h1></p>
				<p>published: 2021-09-26</p>
			</div>
			<hr>
			<div class="box">
				<p>The phrase "considered harmful" in regards to computer science originated in <a href="https://web.archive.org/web/20221212130857/http://www.u.arizona.edu/~rubinson/copyright_violations/Go_To_Considered_Harmful.html">a 1968 essay by Edsger W. Dijkstra</a>, in which he argued that the "go to" statement was harmful because it too easily invited programmers to make an absolute mess of their code. That means, for more than <em>fifty years</em>, computer nerds have been arbitrarily deeming software they don't like, whether they can articulate a proper argument (like the above) or not, "harmful".</p>
				<p>But what does it mean to be "harmful", anyway? Let's open a dictionary (or just dictionary.com) and see:</p>
				<blockquote>harmful: adj. causing or capable of causing harm; injurious: a harmful idea; a harmful habit.</blockquote>
				<p>So <strong>a piece of "harmful" software would be one that caused the user harm or is capable of doing so</strong>. I specify the user because software meant to facilitate piracy might "harm" a corporation's profits, or a tool to break through firewalls might "harm" a control freak's attempt to filter the outside world, but I do not think a reasonable person would consider any of those programs harmful. The user in this sense must also be extended to the computer the user, well, <em>uses</em>, as impairing a person's tools would also impair their ability to complete whatever tasks they were using the tools for, thus harming the user albeit indirectly.</p>
				<p>Right and away, we can consider all malware and viruses to be "harmful" under this definition, for hopefully obvious reasons. If a program is so poorly written that it results in catastrophic data loss or leaks information to parties said information was not intended for, it is harmful because it has done tangible harm to the user. But much like trying to determine what's bloat and what's not, the waters turn murky from here. What makes a program harmful, if not for its actual capacity to do harm to the user? According to the types of people who unironically still use "considered harmful" in Current Year, some of the reasons include:</p>
				<ul>
					<li>complexity of the code</li>
					<li>number of lines of code</li>
					<li>using a programming language the person doesn't like</li>
					<li>having not enough features</li>
					<li>having too many features</li>
					<li><a href="https://archive.md/https://kill-9.xyz/harmful/software/containers">making installation easy for the end user</a></li>
				</ul>
				<p><code>systemd</code> is <a href="http://web.archive.org/web/20210827200448/https://nosystemd.org/">widely</a> <a href="https://archive.md/https://kill-9.xyz/harmful/software/systemd">considered harmful</a> by much of the Linux community, and yet I find it <em>worlds</em> easier to write a service file to daemonize something for <code>systemd</code> than a startup script for any other init system. Much ado has been made about <code>systemd</code>'s supposed myriad bugs, and yet I have never personally encountered any of them. <code>systemd</code> has never caused me any harm, so how could I honestly consider it "harmful"?</p>
				<p>JavaScript is also similarly maligned. It is responsible for much of the corporatization of the internet, facilitating "rich user experiences" like being able to buy things without going into a physical store at the expense of also making possible targeted advertisements, legion browser exploits, cryptocurrency miners, bloated "news" sites that refuse to show any text to, well, <em>text</em>-based browsers or those without JavaScript support... JavaScript demonstrably does the end user much harm when opening only a few tabs can slow their entire machine down to a crawl, but it also means <a href="../february/javascript-good.html">I can run college-mandated software like Microsoft Office without having to actually install it on my computer</a>. (Which, since Office is perennially allergic to WINE, would mean having to install Windows 10 as well.) If I can enable JavaScript when I need to do the aforementioned college tasks and keep it disabled the rest of the time, am I really harmed by it? Has my computing freedom <em>really</em> been damaged?</p>
				<p>However, I would consider <a href="https://archive.md/https://spyware.neocities.org/articles/discord.html">Discord</a> harmful because it demonstrably causes harm to the end user:</p>
				<ul>
					<li>it collects logs of all the system processes running, a MAJOR privacy concern</li>
					<li>it is proprietary software, meaning it is nigh-impossible to verify it <em>doesn't</em> cause the end user harm</li>
					<li>it often requires phone verification, which harms people who don't have phones or use cellular providers blacklisted or not supported by Discord</li>
					<li>"servers" (a false term invented by Discord to mean a collection of related chatrooms) are often shut down without notice, meaning, since both Discord and Reddit have long supplanted the traditional internet forum, information is often lost to time</li>
				</ul>
				<p>Both of my brothers and my one "real-life" friend use Discord. I have tried time and time again to explain why Discord is spyware meant to suck advertising data from them and that they should use software that respects them, but their response is only ever "but all my friends are on it".</p>
				<p>Discord is harming them, but they don't consider it harm because their values are different. A "starvingdev" (the opposite of a "soydev"; one who seeks minimalism at all costs) <a href="https://archive.md/https://kill-9.xyz/harmful/software/python">considers Python harmful</a> because it's "slow" and "bloated", but I do not consider Python a harm to me as it enables me to write software I otherwise would not have as I don't have the attention span to learn a "real" programming language.</p>
				<p>I'd rather spend that time writing poetry, or watering my garden, or riding my bike...</p>
				<p>A Windows user is consistently harmed by Microsoft due to the constant telemetry that cannot be disabled and the updates that take <em>forever</em> to install and, well, Windows just being a pile of shit that crashes a lot. They might concede that having to sit through blue screen after blue screen or update after update is a harm as it prevents them from using their computer for what they bought it for, but I would argue that a Windows user happily making music or Photoshopping to their heart's content is doing them a lot less harm than forcing them to use Linux and forgo the programs they need for their hobbies due to no Linux support for them. In the same vein, I am much happier when my computer setup is Debian set to boot straight to a TTY (as opposed to a graphical session) and I can write in a Byobu session with no tray icons or notifications or other distractions (caused by the computer, anyway) and can <code>startx</code> into i3 for playing games than when I am forced to use a Windows computer for a program with no Linux equivalent, constantly nagged every five minutes with update popups and Cortana begging me to sign in. We have different values and different needs and are harmed when our computers prevent us from fulfilling these.</p>
				<p><strong>The purpose of a computer is to assist the user in completing the tasks they need to do in their life. For a computer program to obstruct the user in this pursuit, or to exploit them in the process, is to do the user harm.</strong> That is, I believe, what "considered harmful" should mean, not anything that falls outside of the cult of ultra-minimalism.</p>
			</div>
			<hr>
			<div class="box">
				<p align=right>CC BY-NC-SA 4.0 &copy; Vane Vander</p>
			</div>
		</article>
	</body>
</html>
