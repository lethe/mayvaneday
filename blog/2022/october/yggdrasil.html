<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Theoretical design for a female-only internet - Archive - MayVaneDay Studios</title>
		<link href="../../../style.css" rel="stylesheet" type="text/css" media="all">
		<meta name="author" content="Vane Vander">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body class="mayvaneday">
		<article>
			<div class="box">
				<h1>Theoretical design for a female-only internet</h1>
				<p>published: 2022-08-06</p>
			</div>
			<hr>
			<div class="box">
				<p>The instructions and ideas contained in this post could technically be used by any group that seeks to fly under the radar of the mainstream "clearnet". I write "female-only" because I started thinking about this after receiving many DMs from women on Ovarit asking if I could potentially in the future provide technical support for their own personal liberation projects.</p>
				<p>A common theme in imageboards and other alt-tech spaces is a feeling that ranges from resentment (at best) to vitriol (at worst) at women as a class for not being as technologically literate as their male counterparts. Never mind that the damn field of study was <a href="https://web.archive.org/web/20221006024237/https://en.wikipedia.org/wiki/Ada_Lovelace">invented by women in the first place</a>. I like to call the resulting spiral "the techbro cycle of exclusion", which goes as follows:</p>
				<ol>
					<li>Men mock women for not knowing about the trendy piece of software <em>du jour</em> or not having the will or time to go looking for alternatives to websites they use daily, such as Facebook.</li>
					<li>Men make tech spaces so misogynistic that the few women who dare to explore beyond "normiedom" go, "Wow, you are all pieces of shit; I don't want to deal with this abuse anymore" and then leave or otherwise withdraw themselves from public view. A chilling effect starts to form where a lack of women in tech makes it harder for other women to gain legitimacy in the space.</li>
					<li>Because becoming more technologically literate comes with the high cost of exposing oneself to the toxic misogyny inherent to techbro culture, few women attain knowledge of liberatory technologies and potentially start to associate things like "open source" with "I'm going to get a bunch of sexual slurs thrown at me just for existing". The chilling effect builds.</li>
					<li>Men liberate themselves while watching women attempting to avoid misogyny inadvertently contribute to their own oppression by remaining ignorant of the technologies becoming more and more critical to the operation of their daily lives.</li>
				</ol>
				<p>If that's too verbose for you, read this draft of the above I scrawled down in the park one day:</p>
				<p><img src="../../../img/TechbroCycle.png" class="big" /></p>
				<p>To counteract this, I've taken it upon myself to singlehandedly populate the <code>o/STEM</code> board of Ovarit with basic tech tutorials and what I feel are the actually good submissions on Hacker News. ("Good" excluding upwards of 99% of the content there, as Hacker News is basically "the tech startup advertising spam website", but that's a complaint for another day.) I don't feel it's productive to yell at random strangers, "What do you mean, you're not running FreeBSD with full-disk encryption and only FOSS software? <em>Clearly</em> you're using your computer wrong!" like how imageboard users like to sling shit at each other over ideological purity in their computing. I don't think the women of Ovarit are stupid. We just have different priorities and interests and hobbies. I chose tech. They chose something else. As the British like to say, "simple as."</p>
				<p>So, keeping in mind that not everybody has the same level of technological knowledge of me (an autistic person having a <a href="https://web.archive.org/web/20221006030338/https://dana.org/article/developing-a-theory-of-mind/">working theory of mind</a>? <em>SHOCKING!!</em>) I set the following constraints when assessing what this "female-only internet" I thought about <a href="../08/separatism-redux.html">a few posts ago</a> would look like:</p>
				<ol>
					<li>The software <em>must</em> already exist, because although I can write a mean Bash script and my knowledge of Python is passable, I don't trust myself to write anything that could potentially be the difference between life and death for someone.</li>
					<li>The software <em>must</em> be available for Windows, and Android if possible, because it's not fair of me to expect the theoretical users of this network to learn how to use Linux or ditch their phones to be stuck at a computer for all communications or learn how to compile a program from source.</li>
					<li>The software <em>must not</em> be more complicated than "run installer and maybe edit a config file and then follow simple directions". While I have an <a href="https://archive.ph/PqTGG" title="Associate of Applied Science, often derogatively referred to as 'a two-year degree'">AAS</a> in Network Systems Administration (yes, I graduated!) and can tolerate software that's a little messy, too much mess and the theoretical users will give up and go back to the clearnet. <strong>So something like <a href="https://web.archive.org/web/20221006030835/http://deavmi.assigned.network/blog/crxn/">CRXN</a> is out.</strong> Also, to a technologically illiterate person, asking them to manually configure network interfaces and set up system daemons (programs that run in the background) looks a little shady, like I'm a scammer priming them to get infected with malware and become part of my botnet.</li>
					<li>The software <em>must</em> be peer-to-peer to avoid, if not completely eliminate, reliance on male-run or otherwise hostile infrastructure. I don't want to spend my life building something that collapses in a single day because I fell victim to the <a href="https://web.archive.org/web/20221004011626/https://en.wikipedia.org/wiki/Bus_factor">bus factor</a>. I also want to sidestep the issue of convincing a bunch of what are essentially strangers to put their entire operations at the mercy of one person (me) who is also to them effectively a stranger. If I die or "turn coats" (never willingly, although an adversary threatening enough could always coerce an admin of <em>anything</em> into revealing info) the network should be able to just collectively cease peering with me, cutting me out of the network, and continue on as usual. <strong>So Tor is out.</strong></li>
					<li>The software <em>must</em> allow each individual user to choose who they peer with in case of schisms in the group. If peering is handled automatically from an external source, the theoretical user risks being connected to a peer outside of the female-only network without their knowledge and accidentally allowing hostile entities to access resources within. <strong>So I2P, Lokinet, and ZeroNet are out.</strong></li>
					<li>The software <em>must not</em> store data on the user's disk that the user did not explicitly ask for. <strong>So Freenet is out</strong> because it both requires massive amounts of disk space (upwards of twenty gigabytes for decent operation) and bandwidth and also because of a problem normies affectionately misreport as "Freenet stores CSAM on your device". Freenet does <em>not</em> store CSAM on your device; it caches <em>encrypted</em> pieces of data that your peers request from elsewhere as they flow through the network, thus allowing frequently-requested content to live longer and be faster to access. <a href="https://web.archive.org/web/20221006034113/https://news.ycombinator.com/item?id=19496825">If you run your Freenet node in "opennet" mode, and your network connection is decent, you're going to connect to random strangers.</a> Some of those may be pedos. Some of those may be law enforcement. Because all data on Freenet is stored and transmitted encrypted, and the only way to decrypt it is to explicitly request it via its URI (whether that be a CHK or an SSK or something else) and then gather enough blocks of it from peers to decrypt it, unless you <em>explicitly</em> tell Freenet to retrieve something nasty and store it unencrypted outside your Freenet datastore, it's essentially just garbage data.</li>
				</ol>
				<p>At this point, our winner is <a href="https://web.archive.org/web/20221004132132/https://yggdrasil-network.github.io/">Yggdrasil</a>, a peer-to-peer overlay network and the spiritual successor to <code>cjdns</code>. The developers describe the project as follows:</p>
				<blockquote>Yggdrasil is an overlay network implementation of a new routing scheme for mesh networks. It is designed to be a future-proof decentralised alternative to the structured routing protocols commonly used today on the Internet and other networks.</blockquote>
				<p>It <a href="https://web.archive.org/web/20221006035646/https://yggdrasil-network.github.io/installation.html">supports Android and iOS</a>, so our mobile friends aren't left out; the Windows installation <a href="https://web.archive.org/web/20221006035819/https://yggdrasil-network.github.io/installation-windows.html">isn't nightmare levels of difficulty</a>, and by default without manual configuration it only connects to other devices within the same local-area network. Tor has its absurdly long <code>.onion</code> addresses and requires you to configure your programs to use it as a proxy, but Yggdrasil operates via a virtual network adapter and so its addresses present themselves to programs like any other IPv6 address. (To be technical for a moment, Yggdrasil addresses come from the <code>0200::/7</code> IPv6 range, which has been <a href="https://web.archive.org/web/20220911142100/https://www.iana.org/assignments/ipv6-address-space/ipv6-address-space.xhtml">deprecated since 2004</a> and doesn't route to anything on the clearnet. So you don't have to worry about routing conflicts.)</p>
				<p>I'll take pieces from my own Yggdrasil configuration files to illustrate the following points.</p>
				<p>As just mentioned, Yggdrasil only connects to peers explicitly defined in its configuration file. So, after a fresh install, the peers section looks like this:</p>
				<p><code>Peers: []</code></p>
				<p>To add peers outside your network, you'd make your peer section look something like this:</p>
				<pre>
Peers:
	[
	tls://01.scv.usa.ygg.yt:443
	tls://lax.yuetau.net:6643
	tls://tasty.chowder.land:9001
	tls://supergay.network:9001
	tls://lancis.iscute.moe:49274
	tls://letsdecentralize.org:1414
]
				</pre>
				<p>All these peers are taken from the <a href="https://web.archive.org/web/20221006040725/https://github.com/yggdrasil-network/public-peers/blob/master/north-america/united-states.md">public peer list</a> that the Yggdrasil developers maintain. <strong>We won't use these in our theoretical female-only network.</strong> The peers listed above are only for the global Yggdrasil testnet currently in operation. In our theoretical network, we would only add peers of other members of the network.</p>
				<p>Because my server has a public IP address (both v4 and v6), to allow others to peer to me I have the <code>Listen</code> section set to the following:</p>
				<pre>
Listen:
[
	"tls://[::]:1414"
]
				</pre>
				<p>"But most of us are behind NAT!" ...Yggdrasil doesn't care. You can have a publically-routable IP; you can be behind one NAT, you can be behind seventeen NATs. If your IP isn't publically routable, or you're otherwise paranoid about maybe <em>possibly</em> having your IP in someone's logs, you can <a href="https://web.archive.org/web/20221006040325/https://github.com/yggdrasil-network/public-peers/blob/master/other/i2p.md">peer with others over I2P</a> or Tor, assuming you have I2P or Tor already installed and properly configured on your device.</p>
				<p>By default, Yggdrasil nodes that have <code>Listen</code> configured will allow anyone to connect to them. In our scenario, we only want peers we know and trust to connect to us, so we find the section that says <code>AllowedPublicKeys</code> and make it look like the following:</p>
				<pre>
AllowedPublicKeys:
[
	9bddf88af964729f2c9ef56b8b6f74d510fc2ede7261f52a293d90916b29f256
	d2960e6b547c9c6fe62127e93c8963b0844af3c07178185692c56fefc5259dc9
]
				</pre>
				<p>For a mutual connection, both parties give each other their public keys. They repeat this for every person they want to connect to. (As long as you keep your config file backed up somewhere safe, you don't have to worry about your keys suddenly changing and losing all your peerings.) Yggdrasil will automatically send traffic between nodes through the most efficient route possible. If two nodes want to communicate but aren't directly connected for some reason, Yggdrasil will send the traffic through whatever nodes are intermediary on the shortest path known. All traffic between nodes is encrypted, even if using an otherwise-plaintext protocol like plain HTTP, so there is no need to worry about a man-in-the-middle attack so long as you make sure you <strong>DON'T SHARE YOUR PRIVATE KEYS WITH ANYONE</strong>.</p>
				<p>With the configuration file edited in the ways described above, the network would only consist of those peers that had opted in. (Verification that prospective users fit the purposes of the network would be up to someone else, as I don't think I'd be fit to be doing lots of voice or video calls with strangers.)</p>
				<p>Yggdrasil by itself is just a overlay network. Even though the routing is peer-to-peer, actual services still follow the client-server model. <a href="https://web.archive.org/web/20221006042505/https://yggdrasil-network.github.io/services.html">The global Yggdrasil testnet has many services operating on it already</a>, including websites, IRC chatrooms, wikis, and even BitTorrent trackers. In our theoretical female-only network, the admins would run some of these themselves on their own servers as entry points, and users would be free to run whatever else they wanted on their own nodes, made available to the rest of this theoretical network. (There can be issues when trying to host multiple websites on port 80 from the same Yggdrasil-generated IPv6 address, but that's beyond the scope of this post.) I mean, I sure <em>hope</em> they would run things... I'd be pretty bored if my website was the only one I could visit on there. The HTML doesn't even have to be pretty. Users who don't have computers / devices they can leave running 24/7 can hop on a <a href="https://web.archive.org/web/20221006044011/https://docs.joinplu.me/installation/with/prebuilt/">hosted blogging service run by another user</a> (likely an admin, although anyone in the network is free to run whatever) and publish their writings there.</p>
				<p>Imagine what the users will feel free to share amongst themselves without the stress of the surveillance endemic to the clearnet...</p>
				<p>What bounties will spring from this new Second Realm?</p>
			</div>
			<hr>
			<div class="box">
				<p align=right>CC BY-NC-SA 4.0 &copy; Vane Vander</p>
			</div>
		</article>
	</body>
</html>
