<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Urbit is still basically just a broken computer simulator - Archive - MayVaneDay Studios</title>
		<link href="../../../style.css" rel="stylesheet" type="text/css" media="all">
		<meta name="author" content="Vane Vander">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body class="mayvaneday">
		<article>
			<div class="box">
				<h1>Urbit is still basically just a broken computer simulator</h1>
				<p>published: 2023-10-01</p>
			</div>
			<hr>
			<div class="box">
				<h2>Recap</h2>
				<p><a href="../../2022/08/urbit.html">I wrote a post about Urbit before</a>, so I don't feel like rehashing the entire intro for what is essentially just an update post, but in case you're too lazy to read it, here's a reminder: Urbit is <a href="https://web.archive.org/web/20230921194025/https://wejn.org/2021/02/urbit-good-bad-insane/">a single-threaded interpreter for webapps that claims to be peer-to-peer</a> but in practice can't connect to other peers half of the time <em>and also</em> falls over if you don't have stable DNS or IPv4 connectivity:</p>
				<pre>
ames: czar at wet.urbit.org: not found (b)
http: fail (14691, 504): temporary failure
http: fail (14692, 504): temporary failure
http: fail (14693, 504): temporary failure
http: fail (14694, 504): temporary failure
http: fail (14695, 504): temporary failure
http: fail (14696, 504): temporary failure
http: fail (14697, 504): temporary failure
				</pre>
				<p>In other words, Urbit is a program you run on your personal computer to make a fashion statement about sticking it to MEGACORP and being self-sovereign while failing to appreciate the irony of a peer-to-peer system that can't bootstrap without a connection to <code>urbit.org</code> and the Ethereum blockchain. You know, since <a href="https://web.archive.org/web/20230921195217/https://urbit.org/overview/urbit-id">Urbit uses Ethereum for identity management</a>. In fact, if you ever lose all the files in your Urbit <a href="https://web.archive.org/web/20230921195651/https://developers.urbit.org/reference/glossary/pier">"pier"</a> (installation), you need to pony up a small amount of ETH (for me it was 0.004) to make the network "forget" you and start again. Since I wasn't planning on using Urbit ever again after my aforementioned post, I had to spend about $7 for this "<a href="https://web.archive.org/web/20230921195848/https://operators.urbit.org/manual/id/guide-to-resets">factory reset</a>". Thankfully whoever had given me my Urbit keys in the first place had left some money in them, so I didn't actually have to spend any of my own money. Urbit also has a distinct lack of actually useful apps for people who don't like to socialize online: in the course of doing research for this update post, I only found one or two apps I could actually see myself using more than once, and those apps already have far superior offline equivalents.</p>
			</div>
			<hr>
			<div class="box">
				<h2>*youtuber voice* LeT's GeT rIgHt InTo It!!1!</h2>
				<p>In my original post, I complained:</p>
				<blockquote>Unintuitively, "installing" more apps means opening the search bar instead of a dedicated "install" button (even a little plus sign would have been more helpful) and pasting in a long unmemorable string from an external app directory...</blockquote>
				<p>Sometime between then and now, this got fixed. When signing into the Urbit web interface, titled "Landscape", at the top of the screen is now a little blue button with the text "Get Urbit Apps". Clicking this button brings up a small app catalog with categories like "Tlon Selects", "Powered by Pals", "Make Urbit Useful", and "Make Urbit Fun". Installing apps takes a while, and the spinning wheel icon remains, but now you get <em>some</em> idea of what Landscape is doing while it loads the relevant app store entry. The Urbit documentation doesn't make this obvious, but you can also install apps via the CLI "dojo": app shortcode <code>~sitden-sonnet/channel</code> translates to the command <code>|install ~sitden-sonnet %channel</code>.</p>
				<p>That doesn't mean app installation is any <em>faster</em>, as you will be often met with this screen for five minutes or more:</p>
				<p><img class="big" src="../../../img/urbit2/lag.png"></p>
				<p>Even if you find an app you want to install, whether in the catalog or on <a href="https://letsdecentralize.org/rollcall/urbit.csv">an app directory elsewhere</a>, you're almost certainly going to run into the same five logs over and over with no success:</p>
				<ol>
					<li>Checking connection</li>
					<li>Checking DNS</li>
					<li>Checking our galaxy</li>
					<li>Checking ~person's sponsors</li>
					<li>~person's sponsor can reach them, but we can't</li>
				</ol>
				<p>Is my Internet connection flaky, or is a good portion of the Urbit community gone without a trace? Who knows! Let's look at some apps still functioning to see what function Urbit has in Current Year.</p>
				<h3>Groups</h3>
				<p>This isn't the titular "glorified chatroom" of my previous post, but it might as well be, because I couldn't connect to the Urbit peer responsible for distributing <code>~dister-fabnev-hinmur/escape</code>. Visually it's a Discord clone, where each group has multiple channels on the left sidebar. But it also takes several minutes to load any messages after you switch to any channel, and my fan was on the maximum speed the whole time I had the Groups tab open. Firefox even asked me multiple times to kill the tab because it was slowing down the rest of my computer.</p>
				<p>Not having any idea where to go, and the man who originally gave me my Urbit keys no longer with an active Internet presence (and thus unavailable for recommendations), I joined one of the default groups named "Tlon Local". The "Internet Cafe" channel was active, and every time I sent a message, someone would reply within an hour or so. Annoyingly, replies to messages don't show up as their own messages but instead sit behind a "replies" button that has to be expanded every time. So unless it's your message, there's no visual reminder that a discussion is still going on... <em>somewhere</em>. Happy backscrolling! I hope it's winter where you are, because your computer is about to turn into a really inefficient space heater.</p>
				<p>Additional groups could be found by scrolling down to the bottom of the channel list in Tlon Local and clicking on "Community Catalog". I was met with a lot of... white squares. And then the squares loaded, and I could see the actual preview images for each channel behind the channel's name, like "stereosound" and "California Art Community". I clicked on one, and after multiple page refreshes I managed to get into a few groups to explore.</p>
				<p><img class="big" src="../../../img/urbit2/group.png"></p>
				<h3>pals (<code>~paldev/pals</code>)</h3>
				<blockquote>Keep track of your friends! Other applications can use this list to find ships to talk to, pull content from, etc.</blockquote>
				<p>Pals looks like a tool to pin peers to fetch Urbit content from. Some Urbit apps use Pals to populate your feed with content. I saw two peers in my list, both with minuses by them, which meant I'd added them but they hadn't added me back.</p>
				<h3>basket (<code>~nodmyn-dosrux/basket</code>)</h3>
				<p>Basket is <em>supposed</em> to be a meme repository. But it won't let you upload images directly to Urbit; you have to instead hand over a URL to an existing image on the Internet. Once you have, the image appears fullscreen, and there are two buttons on the top: "live" and "repo". There's also a search box, but it only appears to search the memes you yourself had added. I didn't see a mechanism to look at the memes of others. So it's like a really shitty <a href="http://web.archive.org/web/20230921202954/https://hydrusnetwork.github.io/hydrus/">Hydrus</a>?</p>
				<p>The images are still served directly from the original URL, by the way. Decide on your own if this is a privacy concern.</p>
				<p><img class="big" src="../../../img/urbit2/meme.png"></p>
				<h3>Goals (<code>~dister-dozzod-niblyx-malnus/gol-cli</code>)</h3>
				<p>Goals is a todo list with too many menus. The first thing you see upon launch being a button labeled "Add a pool to get started". To make a new goal, you hover over the pool, click on the "plus" icon that appears by it, and then type in a new goal and hit the enter key. Hovering over by the goal, a meatball menu appears where you can mark the goal as "complete". Also in the meatball menu are options for inviting other people and groups to either have read-only access to your task pool or to collaborate with adding tasks and marking them as complete.</p>
				<p>Sorry I couldn't think of anything snarky for this one. The app works. That's all I can say.</p>
				<h3>%blog (<code>~hanrut-sillet-dachus-tiprel/blog</code>)</h3>
				<p>I opened <code>%blog</code> and was immediately met with a Markdown post editor. No real tutorial, no setup: you wanted to write a post, so here, write the damn post. My first instinct was to hit Control-A to clear all the lorem ipsum and paste my own words in, but apparently that was the wrong thing to do: it kicked me to a menu with buttons for "new draft" and "new theme". After a few minutes hopelessly lost, I discovered that I could go back to my draft post by clicking the "four arrows" button in the bottom-right corner. A text box lets you type in the desired "slug" (the name of the post in the URL) for the post, so because the poem is titled "A New Page", I picked the slug <code>/a-new-page</code>.</p>
				<p>After I had copy-pasted <a href="../../../poetry/a/access.txt">the same poem I always do when testing web3 dreck</a>, I clicked "Publish"... and then it asked me for a URL:</p>
				<blockquote>You have not yet specified the URL of your blog, please confirm the below location.<br>Subscribers to your blog require this in order to create the correct links.</blockquote>
				<p>It doesn't give you an indication of what you're supposed to put in. But you'd think that app makers would set reasonable defaults, right? So I just hit the publish button again. As it turns out, <a href="https://web.archive.org/web/20230922002818/https://github.com/worpet-bildet/blog-ui/issues/30">doing this without setting a slug can destroy your Urbit installation</a>. Unfortunately I set one, so the post doesn't end here.</p>
				<p>I made the same mistake I made last year and assumed that an Urbit publishing app would allow me to publish <em>inside</em> of Urbit. But it turns out that <code>%blog</code> is just a glorified CMS, because I then went to <code>http://127.0.0.1:8081/a-new-page</code> in a new tab and saw my post. You'd think it would be something like <code>http://127.0.0.1:8081/~socleb-fosrut/a-new-page</code> instead, where anyone can go to that URL and see my post, and I could go to <code>http://127.0.0.1:8081/~sampel-palnet/</code> to see <code>~sampel-palnet</code>'s posts, but "I'm expecting too much of Urbit" seems to be a recurring theme. And in order to use <code>%blog</code> as a CMS, you have to set up a web server to reverse-proxy to your Urbit ship, because it's not like Urbit likes putting unobfuscated files on your hard drive:</p>
				<pre>
perthro:~/urbit/socleb-fosrut$ tree -a
.
├── .bin
│   ├── live
│   │   └── vere-v2.11-linux-x86_64
│   └── pace
├── .http.ports
├── .run
├── .urb
│   ├── bhk
│   │   ├── north.bin
│   │   └── south.bin
│   ├── chk
│   │   ├── north.bin
│   │   └── south.bin
│   ├── conn.sock
│   ├── get
│   ├── log
│   │   ├── data.mdb
│   │   └── lock.mdb
│   └── put
└── .vere.lock
9 directories, 12 files
				</pre>
				<p>Where exactly am I supposed to point Caddy to?</p>
				<h3>kenning (<code>~migmeg-fidlen-sillus-mallus/kenning</code>)</h3>
				<blockquote>Memorize passages of text. Every passage is called a "kenning".</blockquote>
				<blockquote>
					<ul>
						<li>Every kenning is assigned a kelvin version based on word count.</li>
						<li>You may test your knowledge by filling in the blanks.</li>
						<li>As kelvin decreases, the number of blanks increases.</li>
						<li>For large passages, split the text into several kens for best results.</li>
					</ul>
				</blockquote>
				<p>Pretty straightforward. I added the first sentence of <em>SCUM Manifesto</em>. It started with a kelvin of 47. I got two words in before I failed. Very shameful of me.</p>
				<p>But <code>%kenning</code> does what few Urbit apps do, which is actually function as intended.</p>
				<h3>Trill (<code>~dister-dozzod-sortug/trill</code>)</h3>
				<p>Remember Bastyon from <a href="../may/web3-3.html">my last web3 post</a>? Trill is like if you got a cheap knockoff of Bastyon from a person who knows that putting blackbirds on random things is an effective way to get you to buy things. Trill is full of people who'd be happier on <del>8chan</del> 8kun but aren't because 8kun doesn't also require you sell your soul to a blockchain beforehand. Trill is also full of people who hate functioning emojis:</p>
				<p><img class="big" src="../../../img/urbit2/emoji.png"></p>
				<p>There are supposedly quote posts, but none of them ever load:</p>
				<p><img class="big" src="../../../img/urbit2/quote.png"></p>
				<p>There's an "attach image" button, but it just threw an error in my browser's console every time I clicked on it and it did nothing else.</p>
				<pre>
index-74ddaac9.js:168 Uncaught (in promise) TypeError: Failed to construct 'URL': Invalid URL
	at VT (index-74ddaac9.js:168)
	at eW (index-74ddaac9.js:168)
	at rn (index-74ddaac9.js:186)
	at Bo (index-74ddaac9.js:186)
	at Object.KB (index-74ddaac9.js:37)
	at IB (index-74ddaac9.js:37)
	at QB (index-74ddaac9.js:37)
	at bk (index-74ddaac9.js:37)
	at nC (index-74ddaac9.js:37)
	at index-74ddaac9.js:37
				</pre>
				<h3>Astrolabe (<code>~dister-midlev-mindyr/astrolabe</code>)</h3>
				<blockquote>Astrolabe is your one-stop shop for exploring Urbit address space. Currently, Astrolabe lets you type in @ps or Azimuth point numbers to look up ships on the network.</blockquote>
				<p>So you can look up anyone and see who their sponsor is and what kind of Urbit ID they have and how many times they've had to pay for a "factory reset" like I did.</p>
				<p>Look, mom, I'm on TV!</p>
				<p><img class="big" src="../../../img/urbit2/astrolabe.png"></p>
				<h3>fafa (<code>~paldev/fafa</code>)</h3>
				<p>A two-factor authentication app. Finally something I might find useful!</p>
				<p>Oh, what does this disclaimer say?</p>
				<blockquote>remember that neither urbit nor this app have had a full security audit yet. avoid adding factors over unencrypted http. make sure to save the recovery codes, and/or add a copy of these factors into an authenticator on earth.</blockquote>
				<p><code>~paldev</code>, I'm sure you're a nice person, but I'd really prefer not to get hacked, thanks.</p>
				<h3>rumors (<code>~paldev/rumors</code>)</h3>
				<p>Rumors is <em>supposedly</em> an app where you type in small bits of text and then others can see them, like a pseudonymous confessions site. I have no way of knowing for sure what this app is supposed to do, because all I ever saw was the header "rumors and gossip" and then a text box. Whatever text I typed into the box disappeared without a trace once I hit the enter button. I was on Urbit long enough to have plenty of peers, but I never saw any "rumors" from anyone else.</p>
				<h3>picture (<code>~paldev/picture</code>)</h3>
				<p>You can upload a picture. The picture then replaces the default icon for the "picture" app in Landscape. That's all it ever did.</p>
				<h3>grove (<code>~dister-dozzod-dalten/grove</code>)</h3>
				<p>It describes itself as a "groups file drive", so it's some kind of file storage with a sharing component. (Again, good luck accessing your files without going through the Urbit interface since the actual underlying file system is just a bunch of bin files.) The default group in the interface is set to <code>~nibset-napyn/tlon</code>, where uploads seemed to be disabled and I couldn't actually make any folders (even though the folder button isn't greyed out like the upload button is) and also the entire "group" was empty. I couldn't test <code>grove</code> further as there was no button to make my own group or indication as to which other Urbit app would allow me to do so.</p>
				<h3>cliff (<code>~paldev/cliff</code>)</h3>
				<p>Cliff is another file browser, but this one actually lets you browse the Urbit filesystem. (You know, the contents of those <code>.bin</code> files Urbit stores everything in.) The top of the page has the file path. After the name of my Urbit planet is a drop-down list that let me see the data my apps had stored. Most apps just had a <code>desk/bill</code>... folder? file? I'm not sure <em>what</em> it was, because <code>%cliff</code> didn't make it obvious, but it didn't have anything interesting in it.</p>
				<p><img class="big" src="../../../img/urbit2/bill.png"></p>
				<p>In the top-right corner of the screen is a pencil icon. I clicked on it, and it brought me to an editor for plaintext files. (And I thought to myself, <em>finally, an Urbit app that's actually useful!</em>) I edited some text, saved it... and crashed <code>%cliff</code>:</p>
				<div id="player-overlay">
					<video preload="auto" controls="controls">
						<source src="../../../img/urbit2/cliff.webm" type="video/webm" />
						<p style="text-align: center;"><em>Either your browser doesn't support embedded videos, or you turned that feature off.</em></p>
					</video>
				</div>
				<p>The actual error in the dojo would have been informative... if I could actually decipher the Hoon programming language:</p>
				<pre>
crud: %request event failed
[%poke %request]
bar-stack
[ i=[i=/gall/use/cliff/0w2.lvVVM/~socleb-fosrut/edit/save t=[i=/dill t=~[//term/1]]]
t=[i=[i=/eyre/run-app-request/~.eyre_0v4.s0t8o.cq8qa.c8oko.r7m9p.gi0sp t=[i=//http-server/0vk.fjgv1/2.745/1 t=~]] t=~[[i=//http-server/0vk.fjgv1/2.745/1 t=~]]]
]
call: failed
[from=2 deletes={} changes={~}]
[%error-validating /]
[%validate-page-fail / %from %mime]
				</pre>
				<h3>Quorum (<code>~dister-dister-sidnym-ladrut/quorum</code>)</h3>
				<blockquote>A choral explanations app (a la Stack Overflow or Quora) for Urbit</blockquote>
				<p>There is no default "knowledge board". You have to join one. And in order to do that, you have to know one exists first.</p>
				<h3>Canvas (<code>~dister-norsyr-torryn/canvas</code>)</h3>
				<p>Remember <code>r/place</code>, or literally any other "social pixel art" game? It's basically that. You have a private canvas by default, and Canvas the app comes pre-loaded with a public map. There are also buttons to create a new canvas or join an existing one.</p>
				<p>Canvas didn't work well for me on mobile, even with the fancy pen stylus my Galaxy S23 Ultra came with, but Canvas works well enough on desktop. I could place pixels and overwrite large sections of the map in just a few seconds with the bucket tool. There was only one snag: the magnifying glass tool doesn't let you zoom in; instead, if you hover over any arbitrary pixel on the canvas, it tells you who most recently changed that pixel to its current value.</p>
				<p><img class="big" src="../../../img/urbit2/canvas.png"></p>
				<h3>Studio (<code>~tirrel/studio</code>)</h3>
				<blockquote>welcome to studio<br>start by creating a site</blockquote>
				<p>It's a blogging platform. And just like <code>%blog</code>, other people can't access your posts over a pure Urbit connection; <a href="https://web.archive.org/web/20230828014239/https://crypto4dummiez.substack.com/p/custom-domain-for-your-planet">you have to have a domain pointing at your Urbit server and a reverse proxy.</a> So, just like the rest of Urbit, it would only be accessible as long as DNS and HTTP are up. Which makes it no equivalent to the "freesites" in Hyphanet as it requires infrastructure outside Urbit. Which makes Studio just another shitty CMS.</p>
				<p>You know, I'm getting real tired of these... You'd think someone with the technical knowledge required to get an Urbit planet up and running would know how to write some simple static HTML and thus wouldn't have the need for a CMS.</p>
				<h3>Portal (<code>~worpet-bildet/portal</code>)</h3>
				<p>Portal is another Twitter clone. The userbase seems to be nicer than Trill's, though. When I opened Portal, above the usual text box where you can write your posts, there was a search bar... with the OpenAI logo next to it? Below that was a set of "tags": "Crypto", "Longform", "Productivity"... Clicking on a tag wouldn't search posts by the words in the search query, but instead with an AI-like prompt: for example, clicking on "Longform" would populate the words "high wordCount" in the search box and then find posts in the feed with, well, a lot of words.</p>
				<p>Oopsies, you can only post images if you connect an S3 bucket... you know, a service owned by MEGACORP... which Urbit was supposed to help people get <em>away</em> from...</p>
				<p><img class="big" src="../../../img/urbit2/s3.png"></p>
				<p>Next to the like button I saw a button to tip Ethereum. You know, in case you want to fund the poster's next factory reset of their Urbit ship when they inevitably corrupt the opaque database files Urbit thought it was a good idea to store all their data in. We're very nice here at Portal, you know?</p>
				<h3>focus (<code>~havdys-nordus-mocwyl/focus</code>)</h3>
				<p>For the love of whatever deity you believe in, <em>mute your speakers</em> (or at least your browser) when launching this app or else it will blow your eardrums out. Why does a simple timer need to make such a harsh noise on startup? Deity only knows.</p>
				<h3>DukeBox (<code>~tagrev-lacmur-lomped-firser/dukebox</code>)</h3>
				<p>A DOS emulator? An <em>emulator</em> for Urbit? How many layers of abstraction are we going for? Who cares! KidPix, here I come-</p>
				<pre>
Internal Server Error
There was an error while handling the request for /apps/dukebox/.
/app/docket/hoon:<[712 7].[712 33]>
/app/docket/hoon:<[711 5].[718 20]>
/app/docket/hoon:<[708 5].[718 20]>
/app/docket/hoon:<[705 5].[718 20]>
/app/docket/hoon:<[703 5].[718 20]>
/app/docket/hoon:<[702 5].[718 20]>
				</pre>
				<h3>Trackur (<code>~dister-finned-palmer/trackur</code>)</h3>
				<p>Trackur bills itself as a "simple fitness tracker". One can track their weight loss and fasting over time. I was able to input a date a few months ago where I weighed myself and discovered that I'd lost twenty pounds since I moved into my apartment. It took a while to save, but it <em>did</em> save without crashing.</p>
			</div>
			<hr>
			<div class="box">
				<h2>in lieu of an obnoxious YouTube outro</h2>
				<p>I'm still not any more interested in a convoluted chat app to talk with strangers than I was last year, and Urbit still appears no more capable of hosting my writings <em>in-network</em> without the need to shell out to other centralized aspects of the pre-existing Internet like DNS. I have to hand it to Urbit: the documentation has gotten <em>slightly</em> better since last year, as has the onboarding process and Landscape, but I still have a difficult time seeing where Urbit belongs in an "Internet apocalypse" plan. Urbit's app installation process depends heavily on DNS and the <code>*.urbit.org</code> servers working and being accessible from your own connection, and since installing someone's app depends on them being online and reachable at the same time you are, Urbit wouldn't function well in a scenario where we try to conserve on electricity by only coming online for a little bit each day to fetch new messages and send ones out, a use case which Hyphanet handles well and almost seems to encourage. And as the post I linked above (not the one <em>I</em> wrote, the other one) explained, Urbit's app updates, when they <em>do</em> reach you, aren't signed and are installed automatically over-the-air. Compare this to a hypothetical partially-airgapped Debian install where nothing happens to your computer unless you explicitly connect to a network and ask for an update or a new program to install.</p>
				<p>Almost a year ago, <a href="../../2022/december/web3-2.html#solid">I covered a similar network called Solid</a>. Like Urbit, Solid is designed for having all your data for everything in one server (preferably under your control) and allowing apps other people wrote to access this data instead of signing up for a bazillion online accounts across a myriad of servers you don't control. If the whole point of Urbit is to be the only place a user needs to go to do everything online under a single identity, then Solid works better for this: Solid doesn't require a blockchain for identity management, doesn't require you to spend money to reset your identity in case of a hard drive crash, and doesn't (currently) harbor any delusions about one day superceding large portions of the modern-day Internet stack. Also Solid can be jerry-rigged to work over Tor or I2P or Yggdrasil or whatever darknet is to come, while Urbit can't.</p>
				<p>Also Solid has a functioning text editor.</p>
				<p>To recap, Urbit is difficult to join and bootstrap, lacks fundamental security features, doesn't reliably install wanted applications, and when apps <em>are</em> installed they are often low-quality if functional at all. Urbit cannot function in a situation where Internet access is limited or nonexistent. Therefore Urbit, despite the past year of improvements, is still nothing more than a broken computer simulator.</p>
			</div>
			<hr>
			<div class="box">
				<p align=right>CC BY-NC-SA 4.0 &copy; Vane Vander</p>
			</div>
		</article>
	</body>
</html>
