You Can't Go Home Again
2021-07-16

***

"Come summer, there will be as much sun
as anyone
could ever want,
and you will have
all the time in the world
to open a book and let the pages unfurl."

But I doubt summer will ever come,
for the winds tug at my hair,
and the rain waits for no one,
and I have now lost more than a year
to someone else's mistake,
to a whole lot of someone elses' fear.

Can I fight against my nature?
Can I resign myself to torture
self-baden, self-scarred,
severed by far
from the home
that is myth,
that was never my own?

I carry within this body an unspeakable name
pointing to where lies eternal spring,
where I could never return
having earned
failure's shame
and the enmity
of the deity
I only ever yearned
to be friends with.
Only in these books
can I unfurl my wings,
can I step once more
in that town I long forsook.

***

CC BY-NC-SA 4.0 (c) Vane Vander
