forgesitaj memoroj
2016-06-07

***

there could be a person in your life
who you feel is the one, will be the one
and then is a stranger in the end
diverting stares across the bus aisle

there could be a person in your life
who is just a stranger now
sitting quietly in math class alone
and end up being your sunrise and sunset

cherished veils fade from white to red with the fights
and then to black again with the silence
crumbles to gray with the moths
and drops in dust after time

how many people share a single neuron in my brain
forgotten to time but still imprinted somewhere
hiding in the chime of a bell, a burnt corner of the world
how many thousands of brains do I reside in

***

CC BY-NC-SA 4.0 (c) Vane Vander
