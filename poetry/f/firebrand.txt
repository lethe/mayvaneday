firebrand
2019-01-01

***

before, in your grief, you say
"everything sucks, and nothing is okay"
just remember how you used to watch the trees sway

in the death- the absence of light
watching the hands of midnight
scrape their twisted twiggy fingers, locked in eternal fight

take heart, little one! remember your name
chosen by yourself, pains taken care that it was not the same
as the people who took joy in you being the one they should defame

do not discard yourself to the tomb just yet and become a recluse
you think there is light there, but it is just a ruse
to detach you from humanity and rob you of your muse

you have far greater things in life still yet to achieve
you have friends, a lover, family, who in you they believe
just remember: for everything, if you insist, have a good reason to leave

if they drag you into the night, don't be afraid to wrack up a storm
take pride in who you are, and in your human form!
a god you are not, and a girl you shall stay
in terms of bodily functions, anyway

do not shy away from the natural state of the human condition
lest you lose grasp on reality and cause your own perdition

remember that there is no such thing as perfection in life
there is always still more work to be done, more things to cause strife

you are destined for greatness! now go, and make peace!
and bring hope for the future to the very least of these!

***

CC BY-NC-SA 4.0 (c) Vane Vander
