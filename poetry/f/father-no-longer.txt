Father No Longer
2021-07-09

***

Father seems like just a figment
of my imagination, a decade
of watching my feelings for him fade away,
of wondering where the bond between us went.

For I remember in the summer days
of longing, how he caught me writing poetry
about my first love, who'd cheated on me,
and flew into a rage
and took away my phone and severed me from my friends
until I knelt at his feet and promised him my verses would end.

But nowadays I spend my time
letting freely flow my Muse's rhymes
without the fear of his censorship
forcing me to choose between "death" and "quit".
Oh, I repeat myself. Both are the same.
How could I ever try to tame
the ocean's tides
that churn inside,
to quell the life
I've built
brick
by brick
all for myself?

You heard my cry. You answered the call.
And you understood how enthralled
I am with words, and how I must oblige
the beating world that churns inside.
Father and I could never see eye-to-eye.
He could never convince me why I should deny
my feelings, my yearnings for a new world,
to silence my soul and let greatness pass by.

***

CC BY-NC-SA 4.0 (c) Vane Vander
