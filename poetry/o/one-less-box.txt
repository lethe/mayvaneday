One Less Box
2021-07-17

***

For five years,
I was a chalice
full of malice
and tears
as I tried to suss out a gender.
Am I fluid?
Am I two or three?
Am I even part of the binary?
Or shall I eschew the glass,
pack it up and address it
to the person who knew me last,
label: return to sender?

The more time I spend alone,
the more unnecessary it feels
to keep others in the know,
the more I realize I'd rather keep,
not concealed,
but not subjected to the public's heat.

The more time I spend in solitude,
the more I feel gratitude
towards past me
for only having "come out" to two or three
with no proclamation,
no decree
of new name and pronouns
to accomodate my long-sought androgyny.

Maybe the reason
why I wished others would perceive
me as nonbinary
was so that they'd see
me not as female
but as human.

***

CC BY-NC-SA 4.0 (c) Vane Vander
