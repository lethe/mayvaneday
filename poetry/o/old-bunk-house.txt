an old bunk house built for two
2020-01-23

***

your love is a rain-soaked roof
sheltered and framed by trees as proof
that the forest still sees you as one of its own
the throned cabin you rest upon

you've been vacant of human attention for years
and your water pipes have dried up, and so have your tears
vines laced with emerald, envy creeping up your spine
choking the life and the love that I used to call mine

your mouth opens, inviting inside
the animals seeking shelter to hide
but the mold has settled deep in your bones
so the animals leave, disgusted; you sit there alone

eventually the rot will reach your mind too
and not even sun's gentle touch will be able to soothe,
outstretched in good grace with gentle cleansing in tow,
the corpse of a heart that once so violently glowed

***

CC BY-NC-SA 4.0 (c) Vane Vander
