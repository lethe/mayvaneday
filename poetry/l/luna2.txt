Luna II
2020-12-11

***

born from mother
bourne out from mother
expelled from warm womb
into the arms of an icy tomb

Luna, what I wouldn't do
for just one more day with you

knot our fingers
not that which lingers
at the end of sunset
expectations unmet

you and I deserved eternity
a world without end, boundless, free

ceil my rib cage
seal inside the rage
that spills forth from every gash
beckons me to do something rash

I'll build what the gods couldn't give
a life only ours to live

***

CC BY-NC-SA 4.0 (c) Vane Vander
