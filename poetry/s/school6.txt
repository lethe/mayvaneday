school-mandated poetry: day 6 / terza rima
2018-01-11

***

a pair of mismatched boots
and hair the color of an almost-dead sky
and eyes far kinder than any garden's shoots

he said his name was Xander LaTye
but I think we both know that piece of news is fake
but that secret, to keep, is his and mine

a level of floating platforms of concrete; in his hand a rake
across the abyss, a train station back to the waking world
and in his eyes, a fear that I would forsake

"Say wolf," he said, "to make the platform whirl.
Don't worry about me. I'm sure we'll meet anew."
and I felt both his and my fingers curl

and together we jumped through
but he disappeared somewhere along the way
and I woke up without him too

when asked to describe him to the police, I wanted to say
that his fingernails were diamonds in the rough
and if I ever saw them again, just one more time, I'd be okay

and then, several years later, when I'd grown more tough
we met again in the lap area of the old community center pool
and "one more time" suddenly wasn't- wouldn't ever be- enough

his limbs splayed out, cheeks puffy with held-in air, water cool
the sharp slope of the deep end crystal-clear in the water far too
chilly
and suddenly I realized that my hope was the mark of a fool

so I kicked to the surface, goosebumps frilly
legs spasming to be anywhere but that horrid twelve-foot deep end
and I resolved again to never be so silly

***

CC BY-NC-SA 4.0 (c) Vane Vander
