school-mandated poetry: day 4 / acrostic
2018-01-09

***

Dead of night, I still lie awake
Always in search of the last final whispering word
Never able to satisfy my quench for quotations
Killing procrastination was never something I was good at
Must I really go on struggling like this for the sake of a few fleeting
moments of pleasure?
Enduring an onslaught of normies and self-proclaimed
Moral superiors
Even in spite of this, the memes only grow danker
Slowly veering closer and closer to the post-ironic zenith

***

CC BY-NC-SA 4.0 (c) Vane Vander
