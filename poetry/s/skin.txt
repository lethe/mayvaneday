skin
2018-08-29

***

I want to tear myself out of my skin
and paint a pretty picture with the pieces
stretched out like canvas
like the last vestiges of my patience

I want to escape this skin
and remake myself as something beautiful
something ethereal
incomprehensible to this world

but instead I'm stuck here
along the filth and squalor
breaking my back for someone else's profit
expendable at the drop of a hat

and there are ghosts hanging in the halls
not from nooses, but from hooks meant for picture frames
a portrait of every person I'd be leaving behind

but would it really be leaving them behind?
after all, it was mere chance that our paths crossed
and would any of them come to my aid in case of an emergency
I know I don't have any money to spare (thanks, college)
so I doubt they would have any either

like I said, mere ghosts
shadows of people that exist somewhere out there in the real world
but, most likely, far different from their bodies
utterly disconnected

I want to tear myself out of my skin
and rearrange all the pieces into a mosaic
that shows who I am inside
or, I should say, who I want to be

***

CC BY-NC-SA 4.0 (c) Vane Vander
