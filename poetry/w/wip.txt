WIP
2020-12-09

***

Many a project
has sat in disused corners of
my laptop in neglect
over these six past years.
A spark of inspiration,
a candle's fire,
quickly muted once I yet again tire
of coating these hands with clay.
No oxygen, no respiration.
Who has time to waste
their life in work?
I just want to play.

It pains me to think
that more than a decade ago,
after had melted the snow,
my family and I would regularly
hop state lines
to visit aging grat-grandmothers
to make sure they were fine.

But one by one they dropped like flies,
and the farms were sold
to repay debts passed down
to us by old farts
who spent themselves into a tizzy
buying things to try to buy our hearts.
I didn't need luxury. I needed love,
and I sure wasn't going to receive
any from a man whose face,
whose voice,
was always grumpy and mean.

I remember that half-finished home,
the exposed framing upstairs where
Family Sarah and I would roam,
trying not to tear our skin
on pink insulation.
Was it full of shards of glass,
or was it not?
We sure did debate about it a lot.

A dear second-cousin
(or something close)
worked hard to finish
her homework early
so we would have time plenty
to play.

And now, on what
was then an impossible day,
I find myself reciprocating,
working myself into a pale clam
to complete my own exams
so my brother and I
have our own free time.

Maybe it is not yet
time for me to leave
this nest and fly,
but I can help him to achieve
a little bit of freedom.
I have to give it an honest try.

***

CC BY-NC-SA 4.0 (c) Vane Vander
