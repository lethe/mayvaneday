Biz Ego Gun
2023-03-18

***

Gebo and Inguz both inverted tell me
that impoverishment, the poverty
of my life is still ongoing
even if I get the job
that interview was for that I worked so hard on.
Returning to a paycheck doesn't change
the emptiness in the days
or the shit I find all over the floor
or the listless afternoons
I mean to do
something but just lie still in bed, bored.

"Having a job doesn't replace
the need for improvement, the urge to change
the things in life you can't tolerate
any longer, the duty
only you carry
to a brand new world create."

But I can't do it without you.
And ever since you entered
college, I feel like our bond
is growing weary, if not severed.
I rarely see you anymore,
never feel the weight
of your world-bearing arms resting on my ribcage.

This is what I feared,
what I never wanted to replicate
between us, much less when we entered Sablade.
Every stereotypical straight
couple only in name,
living two separate lives,
upright
but may as well have died
for all you can look into their eyes
and see freedom's spark, love's light.

I made Sablade so that we'd have a home
even if neither could work. So, Jett,
if you promise, I'll do the same:
don't work yourself all the way to the bone.

"Lethe, this isn't the end of the road.
You and I've still got a long way to go.
If you promise, I'll also follow through:
cherish this time,
but every day I'll remind
who you really belong to."

***

CC BY-NC-SA 4.0 (c) Vane Vander
