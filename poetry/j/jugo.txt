jugo vs juego
2017-01-21

***

gulping down a rancid mouthful of juice
reminded of a memory from elementary school
spending the day after Christmas lying on the floor
stomach cramping, thoughts filled of pastel hamsters
and all the fun times ahead of me

and now I'm in the same spot again
except the times aren't fun and the bed is packed up somewhere
inspections, I'm getting ready to leave a hyperbole house
please don't look for faults in my heart

there are so many things I’ve had to leave behind
old nooks and crannies around the world, desolate and forgotten
nothing like the feeling of the web 1.0 aesthetic
back when the world was just a Crayola website

being a kiddie will get me nowhere
except in the world of adult babies (not a satisfying path to walk down)
but being a script kiddie, however
will net me loads of money and lawsuits

***

CC BY-NC-SA 4.0 (c) Vane Vander
