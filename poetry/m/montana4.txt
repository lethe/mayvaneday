Montana IV
2021-06-26

***

Standing at the riverside,
muddy waters a mirror
as thousands of faces pass by,
their time
here long since ended,
their ghosts hung up to dry
like my brothers' swimsuits.

I am an idiot to think my youth
would last forever.
Squalor
without end, boundless, free in the final
whispering of the mundane life.
And yet I want to be free
of this sheath
of flesh.
I want love.
I want death.
I need a long rest
from the prison of this persona
I've built, brick by brick, around my body.

There's a powerful persistent part of me
that wants to renounce humanity
and disappear forever into the trees.
It's not the end for which I seek,
but there is a haunting dream
that reoccurs at least
once a week
where my higher mind is sealed
away and I wander for years
in that draconic body in some witch's menagerie.

No more wants,
just needs
and simple pleasures
like romping in that river,
bathing in the sunshine,
stomach content with whatever I can find.
No more work,
no more school,
no more debt
or responsibility.
Owned only by myself,
survivalist's hell
my own little heaven.

And, of course, mind robbed of memories
of all the things I shirked,
I suppose that witch's hand gently scritching
the nape of my neck wouldn't hurt.

***

CC BY-NC-SA 4.0 (c) Vane Vander
