Mitad-marida I
2022-06-11

***

Cold summer. A cold heart
beats in my chest
as I from my house depart,
legs stiff, left arm
aching.

Father spoke, "You are going to kill this tree."
It slipped
from his lips
like a prophecy.
Dogs outside my bedroom window gnawing
on the Velouria Bush, Nidhogg,
portent of the Eschaton.
Too short, too squat,
too weakened from the bark not
there anymore
to hang myself from branch's ledge
in hopes of gaining the knowledge
to see this world through to its bitter end.

I kneel before the now-fenced-in stump
and reach forward. My limbs falter.
A bramble or some other thorn from Dead End Shrine
draws a gash through my skin, nature's penknife.
Rivulets of blood stream
down without recognition of pain,
carmine trickles, a river, a flood,
guided by the soft-falling rain
before the altar.

And I pray,

let us reconcile before closes this day.
Dead-End King,
lead me to victimless iniquity.
Lead me to damnation
without hurting a single being
undeserving.

***

CC BY-NC-SA 4.0 (c) Vane Vander
