Montana I
2021-06-22

***

Frivolities of life,
whispers in the other room
about sins uncommitted,
sins unforgiven,
repentance yet to come.

The horizon has long since swallowed the sun,
but the heat's golden glow
remains
on my skin,
harsh cabin lights
a doctor with an x-ray
trying to peer within.

I want to drill into their gaze
and tell them vivisection is unnecessary.
My heart has been dysfunctional
since birth, arrhythmia,
a machine missing a gear.

I need you near
my body
like the ocean needs the moon.
I wish not to subsume
myself into you, but to admit
that, when the nights
grow long
and I find myself wishing for perfect
dark, I hold on
to the memory of your touch
like the desert recalls the rain
and wishes it, wherever it is, well.
I do not need you to complete
me. But you give me the strength
to complete myself, to hold on,
like I promised, until the showers of May.

***

CC BY-NC-SA 4.0 (c) Vane Vander
