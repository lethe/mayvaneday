Morgana
2022-06-07

***

I am a last echo from a world long since shattered,
remade in the image of a man who only yearns
for power, for obliteration
of all that does not please him.
I am told you, with my sister,
are creating a world without end,
a world all her
own. This is the fate of all Meridian gods,
those that did not spring from mankind's evil odds.
In this I am not surprised.
But I am also told that she seeks to defy
her fate, to not allow the world to subsume
her consciousness once it has come into full bloom.
Indeed, in this she has partially
succeeded, if only due to being bound to a corporeal body
in an Inside so far away.
But the clock is ticking, you who lies
at the end of the road, at the point of every line.
if I could, I would proclaim you blessed
and her acquitted from this death sentence.
But I am long since dead, and this echo almost passed.
Time is for you of the essence.
You have proclaimed often that you wish to spend
your whole life with her. Within this year
will come time to make good on your promise.
I have faith success will be assured
if you are there to protect her.
I would ask no less
for my precious sister, my destructive Seliph.

She is going to give a whole new world to you.
My final wish: please, ensure
she can experience it too.

***

CC BY-NC-SA 4.0 (c) Vane Vander
