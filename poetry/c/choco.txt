That Ain't Chocolate, Son
2020-11-15

***

There are nine hedgehogs
in my house. I have nothing
to do with them because, every time
my mother or my brothers
hold one in their hands,
the tiny creature immediately
sets to work
shitting out a log.

I may be evil
and belonging to foreign lands,
but I abhor having
such filth
on my hands.

So tell me, mother,
why do you hate that I
always close my door
when you act as if
everything of mine
is actually yours?

My pad of art paper,
saved for stormy weather,
gone one day
into the paper shredder

to serve as bedding
for ungrateful creatures
who couldn't tell the difference
between a slaughter
and a wedding.

"I don't care
that you're busy having fun
with your brother you usually torment.
That's not the purpose
for which you are meant.
Be a good girl and help him take
the hedgehog wheels upstairs."

How delicious it is to say
that actually I don't have to spend any pains
on those who rob me of the sun:
"Not my pet, not my problem."

Because of them, I have to
live in a house full of
poop and paper shreds and
shit-covered wheels that squeal
at all hours of the night.

Somehow, I don't believe
you're half as "low-income"
as you claim to be.

***

CC BY-NC-SA 4.0 (c) Vane Vander
