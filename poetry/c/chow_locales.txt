Chow Locales
2023-03-02

***

Last night to myself I thought
in midst of writing drought
while laying myself down in bed,
"When will I ever feel better again?"

Swinging on the crests of zig-zag Sowelo,
landing on all fours as low
as they'll go,
close to the ground.
I wake up at midnight in a sweat.
"Just a dream; no need to fret."
Crawled out of bed
on dog hair-frosted floor
with thrashing hunger too loud to ignore.
My brain'd make me eat an entire damn pizza
if I weren't too much of a coward
to operate the oven at this ungodly hour,
and even then, when all's said'n'done
and I've been abandoned by feral fervor,
my stomach would probably either vomit all out or rupture.

Lover takes in her hands my jaw,
peels back my lips to see my fangs long.
My fingers around her wrists, trembles.
Pinpricks of pupils. Fear of going feral.
"Desperate devouring is a fashion you wear well."

Jormungandr and Ouroboros,
masters of yoga, flexible enough to hold the pose
of curling around to bite their own tails.
I'd maybe get halfway there and fail,
collapse in a crumpled heap on the ground.
There are easier ways to have my foot in my mouth.

***

CC BY-NC-SA 4.0 (c) Vane Vander
