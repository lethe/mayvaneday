uncharming veneer
2020-04-19

***

azurite conviction, standing in a field
as you threaten to so violently your fingers wield
to rip from me the life I was entitled
to live as compensation for being denied the wild

to claim that I as your blood am wrong
for refusing to join such a violent throng
to condemn my brothers and sisters in humanity to the fire
just so I could sit by your side and strum some lyre?

for once in my life,
I am glad to know how to exit
for now I have the power to forbid
that dark fate where the world is your slave
and humans mere toys to fulfill your whims

"this is not the future I choose."

mother's compassion, soft fingers across my cheek
as I flagellate myself for being too weak,
to see the end of the world until it was at my front door,
for neglecting the battle until it became a war

"dry your tears
and face your fears,
else risk losing
your best years."

but how are you supposed to fight an enemy
which does not exist,
which *cannot* exist,
which cannot be uttered by your tongue?

how are you supposed to fight back
when any action you take
will ensure your fate is to be hung?

to be strung up in the sky,
a goddess in your own right,
as you speak only in starlight,
denied the chance to say goodbye

***

CC BY-NC-SA 4.0 (c) Vane Vander
