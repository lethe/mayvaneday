Driven To Death
2022-03-09

***

"What's an operating system?"
Whether they were being serious, I could never tell,
but the question always hung over me like death's bell.

And although camp is now disbanded and dead,
still rings in a disused hall in my head
the words penned on whiteboard in striking red:

while all other girls were so much praise shot
about their skills, their quests, their help,
only written for me: "I guess she smiles a lot."

And when I complained that I had put in
more effort but barely anything received,
Mom marched me to apologize
even though in my eyes
I had committed no crime.

Just be happy with what you've got,
with the crumbs we've thrown your way;
never demand the more you're due,
just smile and bear the pain.

Just smile and bear the pain
of being a prototype, forging the way
to brothers to be done right, to be done at all,
listened to, heard, given right to complain,
and you yourself cast aside
to either be shown up or prepared to die.

I've failed the test on three separate times,
so I know for sure I can't legally drive.
If I need to get somewhere, either I catch a ride,
call a bus, or gather my breath
and bike.
But you're driving me to death.
You're running me raw.
Soon, I think, there'll be nothing at all.
Will you love me then, Mother, with Cheshire smile?
A lot of what's praised
and naught else remains.

***

CC BY-NC-SA 4.0 (c) Vane Vander
