Abortion III
2023-05-08

***

I'm not good at math, Father. Help me calculate.
How many rainbow trinkets you give me will equate
acknowledgement of the trauma
you pressed into my psyche
and a sincere non-prompted apology?
How many gifts until the pain is reparate?

Will it take long to punch the numbers in?
I'll wait.

***

CC BY-NC-SA 4.0 (c) Vane Vander
