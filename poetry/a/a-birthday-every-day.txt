a birthday every day
2019-01-10

***

every day, the universe sketches itself anew
like an etch-a-sketch broken by accident from a cousin's fall
if I am made of the same stuff as the stars
then it is my birthday every day

but even if the atoms that make up my body
all somehow- miraculously- came from the same ball of gas
every day these days I keep reinventing myself
so every revolution might as well be another birthday

I still suspect that others are lying about their birthdays
an effortless reach for clout
meaningless numbers on a screen
that could all be extinguished in a moment's breath
like the birthday candles you purport to require

but what is a birthday, anyway?
just a day that marks one more year around the sun
one more year of being on the run
running out of time

***

CC BY-NC-SA 4.0 (c) Vane Vander
