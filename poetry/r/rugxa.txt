rugxa kresto
2016-08-08

***

wandering across the blank expanses
it's midnight, illuminated by faint glows
am I somebody's guardian angel
or the devil
standing at the gateway to happiness?

I've seen enough scars to last a lifetime
scarlet and crimson flowing into a sink
I would shriek for the sources to stop, to save themselves
but they've dug a hole and drowned themselves in

where are your parents?
I'd like to pass them a little letter
"your daughter is in a dark place right now
and needs not the falsely validating lure of a faceless crowd
but the love of those she's grown up around"

blame is a hard thing to pinpoint here
do I blame the supposed victim for caressing
the poisonous grip of little red hearts
or the parents, oversight failing tremendously
this is your flower, your garden wilting
where’s water when you need it?

***

CC BY-NC-SA 4.0 (c) Vane Vander
