No Sustained State Has Ever Existed
(All Empires Fall Eventually)
2020-08-16

***

you can talk of human nature,
how I need a preacher
to tell me I don't deserve infinity,
how I don't deserve to *breathe* and *live free*.

it hurts me to say, I thought you were close
to seeing behind the veil so thin as a ghost.
no matter, no wait: I'll still cease this pain!
no revolution needed to break these chains!

why should I care about the sleepwalking masses
with their corporatist bows and their highway overpasses?
why should I live under tyranny's grasp
just because of the failures of those in the past?

I know not who you spoke to; I know not what you "learned"
to make liberty to you so easily spurned.
I'll go it alone if I can, if I must
walk down this path without anyone I can trust.

your Stalins and Maos and Lenins breathe no more,
but through my veins burns dear Novatore!
I am a Goddess-sent beast, a destroyer of cages,
through my harsh-spoken words and knobby phalanges!

No war but my war,
no cause but my cause,
no power but my power,
no laws but my laws!

***

CC BY-NC-SA 4.0 (c) Vane Vander
