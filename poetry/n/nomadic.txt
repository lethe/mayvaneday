nomadic
2019-01-11

***

for as long as I can remember
I've always been afflicted with wanderlust

from the very beginning
wandering in waist-high grass in the train yard
threatening to run away
to become lost in the titan machines, slowly rusting towards their demise
given my father's blessing

and you were there when the doldrums started
when I met that accursed brown tumult of hair
starting the first sparks to stoke the flame
that would eventually become me

every day
I told you I wanted to run away
and you never listened, did you?

or maybe you just listened too hard.

we thought Neocities would be a fresh start
given a world of our own instead of cookie-cutter templates
then Lucine made a scene
and with light the shards teemed
and freedom just became another pipe dream.

whether from a genuine need for rebirth
or easily-triggered boredom, pervading through
it seems I am destined to never stand still
to never settle down in one place

either grow or not
forever leaving behind link rot
for the next safe place, I have always sought

***

CC BY-NC-SA 4.0 (c) Vane Vander
