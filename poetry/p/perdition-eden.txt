Perdition from the Garden of Eden
2019-08-08

***

Being exiled from the Garden of Eden
for the crime of heresy to the heathens
who dance in the twilight, praise to old gods on their lips
and pass around a jug of man's tears, from which they take delicate
sips

Although I know in a man's arms I shall never stay
I refuse to deny the way
this heavy crown of bone hangs on my head
a weighty reminder to things better left unsaid

to those who would strip my individual rights
in the pursuit of abolishing the male blight:
Why should I believe that, with you, I'll be saved,
when you'll gladly push the human race into the grave?

My art is indicative of no other feelings than mine.
How dare you attempt to claim a piece of my Divine!
To follow a legacy of bitterness and hatred-
No more! I will profane all you hold sacred.

So tell me,
why should I worship Aphrodite?
Me, the loveless, forlorn,
setting out alone on these foreign shores?

Keep your rituals and your tribes.
I refuse to waste my life
subservient to a deity
who would condemn me,
forgetful, to
die.

***

CC BY-NC-SA 4.0 (c) Vane Vander
