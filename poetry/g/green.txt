green
2018-02-22

***

does the rain in Spain really fall in the plain?
what about the rooftops coated in sky's tears
and empty nests flooded and damp to the point of disintegration

I know it's just a silly rhyme
but sometimes I wonder if the plain really isn’t a plain
a line written by the depths of despair

and now someone I hate could possibly know my true name
not the one put on my birth certificate without my wanting
but the one that stays hidden away, locked in a safebox
my golden ticket out of this place

I never wanted to be famous
stalked or revered or worshipped
all I've ever wanted is respect
and a shred of understanding

***

CC BY-NC-SA 4.0 (c) Vane Vander
