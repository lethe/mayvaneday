In The End Of Everything
2022-04-21

***

I stepped outside during work today,
hoping to take a sip of the clouds,
because there was nothing else to do
and inside was boiling,
stifling,
all headaches exhumed.
Dismal sky
and rain light
on its way,
my head cocked, listening
to the wind, hoping to catch a word from you.

A word, maybe, or a song, or a single note.
Your voice always
lifts me up from my lows
and helps me down from my worst highs.
And in this wind, I think, I could take flight
without fear of being caught in a tornado
or taken to lands foreign and unknown
because I know
all roads lead back to you.
In this wind, in this shower,
I could easily disappear.

What if I was wrong all along
and in reality Eris
yearned for my silence
and you gave me all my songs?
Only recently
having learned to read
and literature never being your thing?
Listening to the midnight trees
scrape against my bedroom window
the years of my childhood where you I did not know.
I look back and angel numbers appear everywhere I go
in everything I've ever done.
How loud did you scream, Jett?
How hard did you pound your fists?
How long did you wait
to see what I'd retained,
what slivers of memory still did persist?

The rain pounds harder outside the window,
and if I'd still been standing on the sidewalk,
my jacket would've long since been soaked through.
An absence of birds
making their curves
along the canvas of the sky,
just a not-even-gray as far as possible
can see the eye.
What I would give for the workday to be over
and to be tucked safely in my bed,
resting in the sturdy-yet-soft arms
of my lover.
To know
tomorrow
will be brighter,
kinder,
holding less harms.

And the tornado comes,
uninvited,
and nothing more.

***

CC BY-NC-SA 4.0 (c) Vane Vander
