Edell
2020-04-28

***

take this to heart, my inner soul:
there will never exist a singular thing
that will make you feel whole

it's tempting, I know, to kneel
at the altar of a Spectacle and let them
dictate the emotions you must feel,

the clothes, the countenance, the color schemes,
while you wonder why you only
feel free in your dreams

and it might be a cop-out to
proclaim that lasting meaning can
only be found within *you*,

but you know it to be true.

so while you slave away
in vain hopes that others will
recognize your pain,

remember the burn
of the midnight hands' call
and in your chest let it churn

and one day you'll see
that their wants are less than your needs
and you'll find the courage to leave

***

CC BY-NC-SA 4.0 (c) Vane Vander
